import geodyn_fortan as fortran
from direct_operator import assocLegendrePoly, derivativeThetaAssocLegendrePoly
import numpy as np
import sys
sys.path.insert(0, '')
print(sys.path)


def eval_Plm_at_theta(theta, lmax, norm=1):
    """
    Evaluates the coeffs of the legendre polynomials (and 1st, and 2nd derivative) at a given theta.

    :param theta: angle where to evaluate the Legendre polys (in rad)
    :type theta: float
    :param lmax: Max degree described by the Legendre polys
    :type lmax: int
    :param norm: Optional arg to set the normalisation of the polys (1=Schmidt semi-norm (default), 2=full norm)
    :type norm: int
    :return: 3 numpy.array with the coefs of the Legendre polys, of its 1st derivative and of its 2nd derivative
    :rtype: np.array, np.array, np.array
    """

    nb_legendre_coefs = int((lmax+1)*(lmax+2)/2)

    coefs_plm = np.zeros(nb_legendre_coefs)
    coefs_dplm = np.zeros_like(coefs_plm)
    coefs_d2plm = np.zeros_like(coefs_plm)

    fortran.legendre.plmbar2(coefs_plm, coefs_dplm, coefs_d2plm, math.cos(theta), lmax, norm)

    return coefs_plm, coefs_dplm, coefs_d2plm

N_max = 13
th = 0.2
all_plm, all_dplm, _ = eval_Plm_at_theta(th, N_max)

for n in range(N_max):
    for m in range(n+1):
        k = (m + 1) * m // 2 + n
        print(plm_at_th)
        print(all_plm[k])
        print(dplm_at_th)
        print(all_dplm[k])
        plm_at_th = assocLegendrePoly(m, n, th)
        dplm_at_th = derivativeThetaAssocLegendrePoly(m, n, th)
        assert all_plm[k] == plm_at_th
        assert all_dplm[k] == dplm_at_th
print('finished loop')
