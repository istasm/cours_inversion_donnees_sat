import matplotlib.pyplot as plt
import numpy as np

import cartopy.crs as ccrs


def main():
    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Mollweide())

    ax.coastlines()
    ax.set_global()

    ax.plot(-117.1625, 32.715, 'bo', markersize=7, transform=ccrs.PlateCarree())
    ax.text(-117, 33, 'San Diego', transform=ccrs.PlateCarree())

    plt.show()


if __name__ == '__main__':
    main()