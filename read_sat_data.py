import numpy as np
import cdflib
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from math import *


def cdf_times_to_np_date(times):
    """
    Transform the times in cdf epoch into numpy dates, rounding month to nearest.
    """
    dates = []
    for t in times:
        if t != t:
            # if time is a nan, date should be a nan as well
            dates.append(np.float('nan'))
            continue
        year, month, day = cdflib.cdfepoch.breakdown_epoch(t)[:3]
        if day > 15:
            if month == 12:
                year += 1
                month = 1
            else:
                month += 1
        dates.append(np.datetime64('{}-{:02}'.format(year, month)))
    return dates


def read_observatories_cdf_data(cdf_filename, measure_type, obs, remove_bias_crust=True, huge_sigma=99999):
    """
    Reads the data of observatories file in cdf format.

    :param huge_sigma: Replace Nans in sigma by this big value
    :type cdf_filename: str
    :param cdf_filename: name of the .cdf file
    :param measure_type: measure type MF = Main field, SV = Secular variation
    :param obs: observation GO = Ground Observatories, CH = Champ, SW = Swarm, OR = Oersted, CR = Cryosat , C0= Composite
    """
    possible_obs = np.array(['GO', 'CH', 'SW', 'OR', 'CR', 'CO'])

    if not np.any(obs == possible_obs):  # error test
        raise ValueError('Got {} as obs value, possible values are {}'.format(obs, possible_obs))
    if not np.any(measure_type == np.array(['MF', 'SV'])):
        raise ValueError('Got {} as measure_type, possible values are {}'.format(measure_type, ('MF', 'SV')))

    cdf_file = cdflib.CDF(cdf_filename)

    thetas = 90 - cdf_file.varget("Latitude")  # convert in colatitude

    if np.any(thetas > 180) or np.any(thetas < 0):  # error test
        raise ValueError('angle th {} represent the colatitude, did you use latitudes instead?'.format(thetas))

    thetas = thetas * np.pi / 180 # convert in rad
    phis = cdf_file.varget("Longitude") * np.pi / 180
    radii = cdf_file.varget('Radius') / 1000 # to convert in kms
    assert np.amax(radii) < 10000 and np.amin(radii) > 6000, 'Are you sure radii are stored in meters?'  # error test

    if measure_type == 'MF':
        times = cdf_file.varget('Timestamp')
        Bs, sigma2 = cdf_file.varget('B_CF'), cdf_file.varget('sigma_CF')**2
        if remove_bias_crust and obs == 'GO':
            Bs -= cdf_file.varget('bias_crust')
    elif measure_type == 'SV':
        times = cdf_file.varget('Timestamp_SV')
        Bs, sigma2 = cdf_file.varget('B_SV'), cdf_file.varget('sigma_SV')**2

    sigma2[np.where(np.isnan(sigma2))] = huge_sigma # replace Nans

    err = 'At least some invalid values of sigma correspond to valid values in B, file, measure and obs {}, {}, {}'
    err = err.format(cdf_filename.split('/')[-1], measure_type, obs)

    if obs == 'GO':
        locs = [''.join([l[0] for l in loc]) for loc in cdf_file.varget('Obs')]
        N_VO = len(list(dict.fromkeys(zip(thetas, phis, locs)))) # small trick that counts the number of unique pairs of theta/phi/locs
    else:
        N_VO = len(dict.fromkeys(zip(thetas, phis)).keys()) # small trick that counts the number of unique pairs of theta/phi
    dates = cdf_times_to_np_date(times)
    return dates, Bs

# ------
# I don't understand the meaning of the parameter " radius "
# ------


def slice_for_obs_ids(cdf_filename, radius=False):
    """
    Params:
    cdf_filename: .cdf file
    Returns:
    alldata: dict that contains all data from cdf file 'file', the keys are
              alldata[zvar_name] = zvar_data
    unique_obs_ids: list of the ordered locations of the observatories with their latitude and longitude
    mask_obs_id: dict. Given a location (th, ph) of an observatory, returns the array indices
                 such that data[mask_obs_id[obs_id]] = data from observatories at different times,
                 where data is e.g. B_CF.
    """
    cdf_read = cdflib.CDF(cdf_filename)
    info = cdf_read.cdf_info()
    zvars = info['zVariables']

    # V0 = Virtual observatories GVO = Geomagnetic virtual observatories
    obs_type = 'GO' if 'GObs' in cdf_filename else 'VO'
    err = 'Allowed values for obs_type are GO or VO, got {} instead'.format(obs_type)
    assert (obs_type == 'GO' or obs_type == 'VO'), err
    # -------------------------------------

    alldata = {name: cdf_read.varget(name) for name in zvars}  # alldata is a dictionary : alldata['zVariables'] to get those

    thetas = 90 - alldata['Latitude']  # list of thetas
    phis = alldata['Longitude']  # list of phis

    if obs_type == 'VO':
        if not radius:
            full_obs_ids = np.array([(th, ph) for th, ph in zip(thetas, phis)])
            unique_observatories_ids = list(dict.fromkeys(zip(thetas, phis)))
            print('unique_observatories_ids')
            print(unique_observatories_ids)
            print('\n')
        else:  # This part is never taken in account because radius = False
            radiuss = alldata['Radius']
            full_obs_ids = np.array([(r, th, ph) for r, th, ph in zip(radiuss, thetas, phis)])
            unique_observatories_ids = list(dict.fromkeys(zip(radiuss, thetas, phis)))

    elif obs_type == 'GO':
        # shape locs from numpy array to list of strings containing locations
        locs = [''.join([l[0] for l in loca]) for loca in alldata['Obs']]
        if not radius:
            full_obs_ids = np.array([(th, ph, loca) for th, ph, loca in zip(thetas, phis, locs)])
            unique_observatories_ids = list(dict.fromkeys(zip(thetas, phis, locs)))
        else:  # This part is never taken in account because radius = False
            radiuss = alldata['Radius']
            full_obs_ids = np.array([(r, th, ph, loca) for r, th, ph, loca in zip(radiuss, thetas, phis, locs)])
            unique_observatories_ids = list(dict.fromkeys(zip(radiuss, thetas, phis, locs)))

    N_VO = len(unique_observatories_ids)  # Number of observatories
    print('Number of observatories')
    print(N_VO)
    print('\n')
    print('Number of points')
    print(len(full_obs_ids))
    print('\n')

    # test is too strong since order of obs_ids does not matter
    assert(np.all(unique_observatories_ids == full_obs_ids[:N_VO]))
    # not true for GO since it is composed of unequal series
    assert(len(full_obs_ids) % N_VO == 0)

    # dictionary whose keys are lats and lons.
    # values of the dict returns all index of data corresponding to the location given by the key
    # e.g. if (th_0, ph_0) correspond to the first VO, mask_obs_id[(th_0, ph_0)] = array(0, 300, 600, ...)
    # (th_, ph_1) to the second VO, mask_obs_id[(th_0, ph_0)] = array(1, 301, 601, ...)
    # since there is 300 VOs.
    # therefore, B_CF[masks_obs_id[(0.0, 5.0)] ] returns the MF at latitude = 0 and longitude = 5
    # at all available times
    # For GO obs_id also contains the name of the observatory
    mask_obs_loc = {'MF': {}, 'SV': {}}
    for obs_id in unique_observatories_ids:
        mask_obs_loc['MF'][obs_id] = (full_obs_ids == obs_id).all(axis=1).nonzero()[0]

    # for new brut series, the timestamp_SV has not the same length as the timestamp.
    # This calls for another mask in that case
    if alldata['Timestamp'].shape != alldata['Timestamp_SV'].shape:
        # loop will stop at the end of timestamp_SV
        full_obs_ids_SV = np.array([(th, ph) for th, ph, t_SV in zip(thetas, phis, alldata['Timestamp_SV'])])
        for obs_id in unique_observatories_ids:
            mask_obs_loc['SV'][obs_id] = (full_obs_ids_SV == obs_id).all(axis=1).nonzero()[0]
    else:
        mask_obs_loc['SV'] = mask_obs_loc['MF']
    return alldata, unique_observatories_ids, mask_obs_loc


def change_from_spherical_to_robinson(longitudes: list, latitudes: list):
    """
    Change the latitude and longitude in spherical Earth's base into x,y
    coordinates in Robinson's representation of the Earth.
    latitude and longitude need to have the same size
    Param:
        latitude: list
        longitude: list
    Returns:
        Nothing if latitude and longitude don't have the same size
        lists of coordinates x and y if latitude and longitude have the same size
    """
    A1 = 1.340264
    A2 = -0.081106
    A3 = 0.000893
    A4 = 0.003796
    R_Earth = 6370
    x2 = []
    y2 = []
    x1 = []
    y1 = []
    m = 1
    if len(longitudes) == len(latitudes):
        for k in range(len(longitudes)):
            delta = np.arcsin(sqrt(3) / 2 * np.sin(latitudes[k]))
            x1.append((2 * sqrt(3) * longitudes[k] * delta) / (3 * (A1 + 3 * A2 * delta ** 2 + delta ** 6 * (7 * A3 + 9 * A4 * delta ** 2))))
            y1.append(delta*(A1+A2*delta**2+delta**6*(A3+A4*delta**2)))
            x2.append(R_Earth * longitudes[k] * np.cos(delta) / (m * (A1 + 3 * A2 * delta ** 2 + 7 * A3 * delta ** 6 + 9 * A4 * delta ** 8)))
            y2.append(R_Earth * latitudes[k] * (A1 + A2 * delta ** 2 + A3 * delta ** 6 + A4 * delta ** 8))
        return x2, y2
    else:
        return


if __name__ == '__main__':
    cdf_dir = 'donnees/cdf_files_basic_sync_functions_201'
    swarm_file = os.path.join(cdf_dir, 'SW_OPER_VOBS_4M_2__20140301T000000_20210701T000000_0201_basic_sync_functions.cdf')
    ground_file = os.path.join(cdf_dir, 'GObs_4M_19970301T000000_20211101T000000_0103_basic_sync_functions.cdf')
    # shortcut for swarm
    obs = 'GObs'
    measure_type = 'MF'

    all_data, unique_obs_ids, mask_obs_id = slice_for_obs_ids(ground_file)

    print('observations keys: ', all_data.keys())

    print('locations: ', unique_obs_ids)

    # loc = (107.96135, 8.61782) # choosing a location, latitude, longitude to see the data at the given point
    # all_data['B_CF'] contains the MF at all locations, at all times.
    # mask_obs_id is a dict containing arrays of booleans, where mask_obs_id['MF'][loc] is true for data at the given loc, and wrong elsewhere.
    # print(mask_obs_id.keys())
    # B_at_loc = all_data['B_CF'][mask_obs_id['MF'][loc]]
    # plots the MF at the location
    # plt.plot(all_data['Timestamp'][mask_obs_id['MF'][loc]], B_at_loc[:, 0], label=f'MF at {loc}')

    #  plt.show()
    latitudes = []
    longitudes = []
    for k in range(len(unique_obs_ids)):
        latitudes.append(90 - unique_obs_ids[k][0])
        longitudes.append(unique_obs_ids[k][1])

    print('\nlatitudes')
    print(latitudes)
    print(len(latitudes))
    print('Max')
    print(max(latitudes))
    print('Min')
    print(min(latitudes))
    print(len(latitudes))
    print('\nlongitudes')
    print(longitudes)
    print(len(longitudes))
    print('Max')
    print(max(longitudes))
    print('Min')
    print(min(longitudes))

    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.Mollweide(central_longitude=0))
    for k in range(len(latitudes)):
        ax.plot(longitudes[k], latitudes[k], 'bo', markersize=3, transform=ccrs.PlateCarree())
    ax.coastlines()
    ax.set_global()
    # Scaling problem between the map and the localisation given my the data
    plt.show()




