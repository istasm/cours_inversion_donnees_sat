from gettext import npgettext
import matplotlib.pyplot as plt
import numpy as np
import mpl_toolkits
#  from mpl_toolkits.basemap import Basemap


def sub_map(geo_mis, field, title, abs_diff=True, vmax=None, measure='MF'):
    """
    :param geo_mis: dict containing data points, with positions as keys and MF data as values in the form geo_mis = {(r1, th1, ph1): data1, (r2, th2, ph2): data2 ...}
    """
    #test2

    m = Basemap(llcrnrlon=-180, llcrnrlat=-90, urcrnrlon=180, urcrnrlat=90)

    # get list of thetas and phis
    thetas = [th for r, th, ph in geo_mis.keys()]
    phis = [ph for r, th, ph in geo_mis.keys()]
    phis, thetas = np.array(phis), np.array(thetas)
    
    # if in rad
    phis = phis * (180 / np.pi)
    thetas *= 180 / np.pi
    thetas = 90 - thetas
    
    lons, lats = m(phis, thetas)

    if measure == 'MF':
        plt.set_title(r'$B_{{ {0} }} {1}$'.format(field, title))
    else:
        plt.set_title(r'$\frac{{d}}{{dt}} B_{{ {0} }}, {1}$'.format(field, title))

    field_label = 0 if field == 'r' else 1 if field == 'th' else 2

    m.drawcoastlines()
    m.drawparallels(np.arange(-90, 90, 30), labels=[1, 0, 0, 0])
    m.drawmeridians(np.arange(m.lonmin, m.lonmax+30, 60), labels=[0, 0, 0, 1])

    misfits_scatter = [[M[field_label] for M in geo_mis.values()]]

    cmap = 'OrRd' if abs_diff else 'seismic'
    m.scatter(lons, lats, latlon=True, c=misfits_scatter, s=700, cmap=cmap, alpha=0.5)
    # pos_max = np.argmax(misfits_scatter)

    colorbar_max = np.amax(misfits_scatter) if vmax is None else vmax[field_label]
    colorbar_min = 0 if abs_diff else -colorbar_max
    plt.clim(colorbar_min, colorbar_max)
    plt.show()
