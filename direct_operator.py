from functools import lru_cache
import numpy as np
from scipy.special import lpmv
import math

r_earth = 6371.2
r_iono = 60000
# r_iono ~= 60000


def compute_direct_obs_operator(positions, max_degree, internal_source=True, eps=1e-7):
    """
    Computes the direct observation operator H of a measure of given max degree for a list of given positions.

    :param positions: List of the positions stored under the form [r1, th1, ph1, r2, th2, ph2 ...]
    :type positions: list or 1D numpy.ndarray
    :param max_degree: Maximal degree of the observed measure
    :type max_degree: int
    :param internal_source: whether the Gauss coefficient represent an internal or an external source (a/r or r/a dependency)
    :type internal_source: bool
    :return: the direct observation operator H
    :rtype: 2D numpy.ndarray (nb_observations x nb_coefs)
    """

    # Number of observation parameters
    Nobs = len(positions)
    # Number of Gauss coefficients
    Ncoefs = max_degree * (max_degree + 2)

    H = np.zeros([Nobs, Ncoefs])

    for i in range(0, Nobs, 3):
        r = positions[i]
        th = positions[i + 1]
        ph = positions[i + 2]
        H[i:i + 3, :] = compute_lines_direct_obs_operator(r, th, ph, internal_source, eps, max_degree)
    return H


# The 3 is there to make one loop per observatory
# for the maxsize, counting 300 GVOS for each era + 200 GVOs roughly 2000 independent positions.
# since the cache delete oldest entries since last utilisation, it could be possible to lower maxsize.


@lru_cache(maxsize=2048)
def compute_lines_direct_obs_operator(r, th, ph, internal_source, eps, max_degree):
    """
    Convenience function, as most of the observations positions are similar through time (GVOs are unchanged for each satellite era),
    a cache is used to avoid computing the same coefficients for each time step.
    """
    Ncoefs = max_degree * (max_degree + 2)
    r_th_ph_coefs = np.zeros(shape=(3, Ncoefs))

    # Compute prefactors
    if internal_source:
        radius_to_earth = r_earth / r
    else:
        radius_to_earth = r / r_earth
        a_on_b = r_earth / r_iono

    sin_th = math.sin(th)
    if abs(sin_th) < eps:  # theta is either O or pi
        sin_th_0 = True
        sign_cos_th = np.sign(math.cos(th))
    else:
        sin_th_0 = False

    for k in range(0, Ncoefs):
        m, n, coef = get_degree_order(k)
        # m = degree, n = order, coef = name (g, h)
        k_legendre = (m + 1) * m // 2 + n

        plm_at_th = assocLegendrePoly(n, k_legendre, th)
        dplm_at_th = derivativeThetaAssocLegendrePoly(n, m, th)
        # Evaluate the Legendre polynomials using the scipy function lpmv

        cos_mph = math.cos(n * ph)
        sin_mph = math.sin(n * ph)

        if internal_source:
            if coef == "g":
                r_th_ph_coefs[0, k] = (m + 1) * radius_to_earth ** (m + 2) * plm_at_th * cos_mph
                r_th_ph_coefs[1, k] = - radius_to_earth ** (m + 2) * dplm_at_th * cos_mph
                if sin_th_0:
                    assert sin_mph == 0 or plm_at_th == 0, 'One of these values should be 0, {}, {}'.format(sin_mph, plm_at_th)
                    r_th_ph_coefs[2, k] = radius_to_earth ** (m + 2) * n * sign_cos_th * dplm_at_th * sin_mph
                else:
                    r_th_ph_coefs[2, k] = radius_to_earth ** (m + 2) * n * plm_at_th * sin_mph / sin_th
            else:
                r_th_ph_coefs[0, k] = (m + 1) * radius_to_earth ** (m + 2) * plm_at_th * sin_mph
                r_th_ph_coefs[1, k] = -radius_to_earth ** (m + 2) * dplm_at_th * sin_mph
                if sin_th_0:
                    assert cos_mph == 0 or plm_at_th == 0, 'One of these values should be 0, {}, {}'.format(cos_mph, plm_at_th)
                    r_th_ph_coefs[2, k] = -radius_to_earth ** (m + 2) * n * sign_cos_th * dplm_at_th * cos_mph
                else:
                    r_th_ph_coefs[2, k] = -radius_to_earth ** (m + 2) * n * plm_at_th * cos_mph / sin_th
        else:  # if the source is external to the position, then the dependence in the radius is as (r / a)^l-1 instead of (a / r)^l+2 shown in Sab-aka 2010
            if coef == "g":
                r_th_ph_coefs[0, k] = (m + 1) * a_on_b ** (2 * m + 1) * radius_to_earth ** (
                            m - 1) * plm_at_th * cos_mph
                r_th_ph_coefs[1, k] = (m + 1) / m * a_on_b ** (2 * m + 1) * radius_to_earth ** (
                            m - 1) * dplm_at_th * cos_mph
                assert not sin_th_0
                r_th_ph_coefs[2, k] = - (m + 1) / m * a_on_b ** (2 * m + 1) * radius_to_earth ** (
                            m - 1) * n * plm_at_th * sin_mph / sin_th
            else:
                r_th_ph_coefs[0, k] = (m + 1) * a_on_b ** (2 * m + 1) * radius_to_earth ** (
                            m - 1) * plm_at_th * sin_mph
                r_th_ph_coefs[1, k] = (m + 1) / m * a_on_b ** (2 * m + 1) * radius_to_earth ** (
                            m - 1) * dplm_at_th * sin_mph
                assert not sin_th_0
                r_th_ph_coefs[2, k] = (m + 1) / m * a_on_b ** (2 * m + 1) * radius_to_earth ** (
                            m - 1) * n * plm_at_th * cos_mph / sin_th
    return r_th_ph_coefs


def get_degree_order(k):
    """
    Gets the degree, order and coefficient name ("g" or "h") of a coefficient.
    :param k: index of the coefficient
    :type k: int
    :return: degree, order and coef name of the coefficient
    :rtype: int, int, str
    :rq: !! still need to compute the coef q and s !!
    """

    assert (int(k) == k and k >= 0)

    # For m = 0, k = l**2 -1.
    floating_sqrt_result = math.sqrt(k + 1)
    degree = int(floating_sqrt_result)
    if degree == floating_sqrt_result:
        return degree, 0, "g"

    # We need now to find m verifying:
    #    for g : k = l**2 + 2m - 2 => 2m = k - l**2 + 2
    # OR for h : k = l**2 + 2m - 1 => 2m = k - l**2 + 1

    twice_order = k - degree * degree + 2
    if twice_order % 2 == 0:
        coef = "g"
    else:
        coef = "h"
        # If twice_order is not divisible by 2, it means that the 2m is in fact (twice_order - 1)
        twice_order = twice_order - 1

    order = twice_order // 2
    return order, degree, coef


Rc = 3485.0 * 10 ** 0  # CMB radius (km)
Ra = 6371.2 * 10 ** 0  # Earth's reference radius (km)


# =============================================
# calculation norm coefficient for QN Shmidt
# =============================================

def norm_coef(l, m):
    if m == 0:
        return 1
    else:
        return np.sqrt(2 * ((math.factorial(l - m)) / (math.factorial(l + m))))


# =============================================
# calculation norm coefficient full norm
# =============================================

def full_norm(l, m):
    return np.sqrt(2 * l + 1) * np.sqrt(np.math.factorial(l - m) / np.math.factorial(l + m))


# =============================================
# Associated Legendre functions Pn,m(x)
# =============================================

def assocLegendrePoly(m, n, theta):
    Plm = lpmv(m, n, np.cos(theta)) * ((-1) ** m)
    return Plm


# =============================================
# Derivative of Associated Legendre functions dPn,m(x) / dtheta
# =============================================

def derivativeThetaAssocLegendrePoly(m, n, theta):
    Plm = (n + 1) * (-np.cos(theta) / np.sin(theta)) * lpmv(m, n, np.cos(theta)) + \
          (n - m + 1) * (1 / np.sin(theta)) * lpmv(m, n + 1, np.cos(theta))
    return Plm * ((-1) ** m)


# =============================================
# Second Derivative of Associated Legendre
# functions d^2 Pn,m(x) / dtheta^2
# =============================================

def secondDerivativeThetaAssocLegendrePoly(m, n, theta):
    Plm = (n + 1) * ((n + 1) * lpmv(m, n, np.cos(theta)) * np.cos(theta) -
                     (n - m + 1) * lpmv(m, n + 1, np.cos(theta))) * np.cos(theta) + \
          (n + 1) * lpmv(m, n, np.cos(theta)) * (np.sin(theta) ** 2) + \
          (n + 1) * lpmv(m, n, np.cos(theta)) * (np.cos(theta) ** 2) + \
          ((n + 2) * lpmv(m, n + 1, np.cos(theta)) * np.cos(theta) -
           (n - m + 2) * lpmv(m, n + 2, np.cos(theta))) * \
          (-n + m - 1) - (n - m + 1) * lpmv(m, n + 1, np.cos(theta)) * np.cos(theta)
    return Plm * ((-1) ** m) / (np.sin(theta) ** 2)


# =============================================
# Third Derivative of Associated Legendre
# functions d^2 Pn,m(x) / dtheta^2
# =============================================


def thirdDerivativeThetaAssocLegendrePoly(m, n, theta):
    Plm = ((-1) * (n + 1) * ((n + 1) * lpmv(m, n, np.cos(theta)) * np.cos(theta) -
                             (n - m + 1) * lpmv(m, n + 1, np.cos(theta))) * (np.cos(theta) ** 2) -
           (n + 1) * ((n + 1) * ((n + 1) * lpmv(m, n, np.cos(theta)) * np.cos(theta) -
                                 (n - m + 1) * lpmv(m, n + 1, np.cos(theta))) * np.cos(theta) +
                      (n + 1) * lpmv(m, n, np.cos(theta)) * (np.sin(theta) ** 2) +
                      ((n + 2) * lpmv(m, n + 1, np.cos(theta)) * np.cos(theta) -
                       (n - m + 2) * lpmv(m, n + 2, np.cos(theta))) * (-n + m - 1)) * np.cos(theta) -
           (2 * (n + 1) * ((n + 1) * lpmv(m, n, np.cos(theta)) * np.cos(theta) -
                           (n - m + 1) * lpmv(m, n + 1, np.cos(theta))) - (n - m + 1) * lpmv(m, n + 1, np.cos(theta))) *
           (np.sin(theta) ** 2) + ((n + 2) * lpmv(m, n + 1, np.cos(theta)) * np.cos(theta) -
                                   (n - m + 2) * lpmv(m, n + 2, np.cos(theta))) * (n - m + 1) * np.cos(theta) +
           (n - m + 1) * ((n + 2) * ((n + 2) * lpmv(m, n + 1, np.cos(theta)) * np.cos(theta) -
                                     (n - m + 2) * lpmv(m, n + 2, np.cos(theta))) * np.cos(theta) +
                          (n + 2) * lpmv(m, n + 1, np.cos(theta)) * (np.sin(theta) ** 2) +
                          ((n + 3) * lpmv(m, n + 2, np.cos(theta)) * np.cos(theta) -
                           (n - m + 3) * lpmv(m, n + 3, np.cos(theta))) * (-n + m - 2)) -
           2 * ((n + 1) * ((n + 1) * lpmv(m, n, np.cos(theta)) * np.cos(theta) -
                           (n - m + 1) * lpmv(m, n + 1, np.cos(theta))) * np.cos(theta) +
                (n + 1) * lpmv(m, n, np.cos(theta)) * (np.sin(theta) ** 2) +
                (n + 1) * lpmv(m, n, np.cos(theta)) * (np.cos(theta) ** 2) +
                ((n + 2) * lpmv(m, n + 1, np.cos(theta)) * np.cos(theta) -
                 (n - m + 2) * lpmv(m, n + 2, np.cos(theta))) * (-n + m - 1) -
                (n - m + 1) * lpmv(m, n + 1, np.cos(theta)) * np.cos(theta)) * np.cos(theta))

    return Plm * ((-1) ** m) / (np.sin(theta) ** 3)
