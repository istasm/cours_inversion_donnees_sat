import h5py
import numpy as np

with h5py.File('donnees/CHAOS-7.hdf5') as file:
    gauss_coeffs = np.array(file['gnm'])

print(gauss_coeffs.shape, gauss_coeffs)